### Custom errors 💥

Ejemplo básico para presentar una definición básica de cómo dividir errores de dominio y errores "externos" con respuesta HTTP. La estructura sigue una pseudo arquitectura hexagonal, simplificada lo suficiente como para demostrar el punto de la división (por esta razón también los unit tests están sólo del lado del pkg de errores)

La definición de estos errores "custom" está en [custom_errors](utils/custom_errors).

