package cmd

import (
	"net/http"

	"gitlab.com/paulaolmedo/custom_errors/internal/application"
	"gitlab.com/paulaolmedo/custom_errors/internal/handlers"
	customerrors "gitlab.com/paulaolmedo/custom_errors/utils/custom_errors"
)

func init() {
	setupCodes()
	setupAPI()
}

func setupCodes() {
	errorCodes := map[string]int{
		"some_useful_code": http.StatusTeapot,
	}

	customerrors.SetCodes(errorCodes)
}

func setupAPI() {
	helloUseCase := application.NewHello()
	helloHandler := handlers.NewHello(helloUseCase)
	helloHandler.Startup()
}
