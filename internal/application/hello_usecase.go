package application

import (
	"errors"

	customerrors "gitlab.com/paulaolmedo/custom_errors/utils/custom_errors"
)

// HelloUseCase
type HelloUseCase interface {
	// Reply método único de la definición de caso de uso, utilizado para devolver un "error de dominio2"
	Reply() error
}

type hello struct {
}

// NewHello inicializa HelloUseCase
func NewHello() HelloUseCase {
	return &hello{}
}

func (p *hello) Reply() error {
	err := errors.New("internal generated error")
	return customerrors.NewDomainError("Some business description", "some_useful_code", err)
}
