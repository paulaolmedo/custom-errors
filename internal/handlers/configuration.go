package handlers

import (
	"github.com/gin-gonic/gin"
	customerrors "gitlab.com/paulaolmedo/custom_errors/utils/custom_errors"
)

// Startup inicialización de la API
func (h handler) Startup() {
	router := gin.New()
	router.Use(errorHandler())
	router.GET("/hello", h.hello)
	router.Run(":8083")
}

// errorHandler middleware para convertir errores de dominio en errores "externos"
func errorHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()
		for _, err := range ctx.Errors {
			externalError := customerrors.NewExternalError(err)
			ctx.AbortWithStatusJSON(externalError.HttpStatus, externalError)
		}
	}
}
