package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/paulaolmedo/custom_errors/internal/application"
)

type handler struct {
	helloUseCase application.HelloUseCase
}

// NewHello inicializa handler básico
func NewHello(h application.HelloUseCase) handler {
	return handler{helloUseCase: h}
}

func (h handler) hello(ctx *gin.Context) {
	if err := h.helloUseCase.Reply(); err != nil {
		ctx.Error(err)
		return
	}

	ctx.JSON(http.StatusOK, "esto es de compromiso porque nunca llega a este punto")
}
