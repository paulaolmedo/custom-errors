package errors

import (
	"errors"
)

// DomainError custom structure that represents an error
type DomainError struct {
	Inner       error
	Description string
	Code        string
}

// Error method taken from the original error package implementation.
// It allows DomainError to be treated as an error
func (d DomainError) Error() string {
	if d.Inner == nil {
		return "unspecified error"
	}

	return d.Inner.Error()
}

// Unwrap method taken from the original error package implementation
// It allows the user to use methods such as Is and As
func (d DomainError) Unwrap() error {
	return d.Inner
}

// DomainError para errores propios del dominio (ej en application, en dbstorage)
func NewDomainError(description string, code string, inner error) *DomainError {
	return &DomainError{Description: description, Code: code, Inner: inner}
}

// IsAKnownDomainCode checks if the domain code is a registered one
func IsAKnownDomainCode(code string, err error) bool {
	var domainError *DomainError
	if !errors.As(err, &domainError) {
		return false
	}

	if code != domainError.Code {
		return false
	}

	return true
}
