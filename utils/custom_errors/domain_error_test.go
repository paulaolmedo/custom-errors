package errors_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	customerrors "gitlab.com/paulaolmedo/custom_errors/utils/custom_errors"
)

func TestIsAKnownDomainCode_WhenTheCodeIsKnown(t *testing.T) {
	domainError := customerrors.NewDomainError("some message", "known_code", errors.New("forced error"))

	assert.True(t, customerrors.IsAKnownDomainCode("known_code", domainError))
}

func TestIsAKnownDomainCode_WhenTheCodeIsUnknown(t *testing.T) {
	domainError := customerrors.NewDomainError("some message", "unknown_code", errors.New("forced error"))

	assert.False(t, customerrors.IsAKnownDomainCode("known_code", domainError))
}

func TestIsAKnownDomainCode_WhenTheErrorIsNotADomainError(t *testing.T) {
	notADomainError := errors.New("forced error")

	assert.False(t, customerrors.IsAKnownDomainCode("known_code", notADomainError))
}

func TestErrorGeneration_WhenTheInnerErrorIsSpecified(t *testing.T) {
	domainError := customerrors.NewDomainError("some message", "known_code", errors.New("specified error"))

	assert.EqualError(t, domainError, "specified error")
}

func TestErrorGeneration_WhenTheInnerErrorIsNotSpecified(t *testing.T) {
	domainError := customerrors.NewDomainError("some message", "known_code", nil)

	assert.EqualError(t, domainError, "unspecified error")
}
