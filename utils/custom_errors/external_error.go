package errors

import (
	"errors"
	"net/http"
	"sync"
)

const (
	internalErrorCode = "internal_error"
)

var (
	domainErrorCodes map[string]int
	once             sync.Once
)

func SetCodes(d map[string]int) {
	once.Do(func() {
		domainErrorCodes = d
	})
}

// ExternalError custom error to use in the http layer
type ExternalError struct {
	// Code is an internal reference of all the possible errors in the application
	Code string `json:"code"`
	// BusinessDescription of the error
	BusinessDescription string `json:"description"`
	// HttpStatus of the error. Must be ignored
	HttpStatus int `json:"-"`
}

func (e ExternalError) Error() string {
	return e.BusinessDescription
}

// NewExternalError initializes a handler error
func NewExternalError(err error) ExternalError {
	description := err.Error()
	code := internalErrorCode

	var domainError *DomainError
	if errors.As(err, &domainError) {
		description = domainError.Description
		code = domainError.Code
	}

	statusCode := checkErrorCode(code)

	return ExternalError{
		Code:                code,
		BusinessDescription: description,
		HttpStatus:          statusCode,
	}
}

// checkErrorCode recibe el código "interno"
// correspondiente a las distintas partes del dominio, y lo mapea con un código http
func checkErrorCode(code string) int {
	if code, ok := domainErrorCodes[code]; ok {
		return code
	}

	return http.StatusInternalServerError
}
