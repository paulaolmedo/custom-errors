package errors_test

import (
	"errors"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	customerrors "gitlab.com/paulaolmedo/custom_errors/utils/custom_errors"
)

func TestMain(m *testing.M) {
	testErrors := map[string]int{"invalid_input": http.StatusBadRequest}
	customerrors.SetCodes(testErrors)

	os.Exit(m.Run())
}

func TestExternalError_IsCreatedSuccessfully_WithAStantardError(t *testing.T) {
	externalError := customerrors.NewExternalError(errors.New("forced error"))

	assert.NotEmpty(t, externalError)
	assert.Equal(t, "internal_error", externalError.Code)
	assert.Equal(t, "forced error", externalError.BusinessDescription)
	assert.Equal(t, http.StatusInternalServerError, externalError.HttpStatus)
}

func TestExternalError_IsCreatedSuccessfully_WithADomainError(t *testing.T) {
	domainError := customerrors.NewDomainError("some message", "invalid_input", errors.New("forced error"))

	externalError := customerrors.NewExternalError(domainError)

	assert.NotEmpty(t, externalError)
	assert.Equal(t, "invalid_input", externalError.Code)
	assert.Equal(t, "some message", externalError.BusinessDescription)
	assert.Equal(t, http.StatusBadRequest, externalError.HttpStatus)
}
